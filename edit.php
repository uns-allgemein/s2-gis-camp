<?php
$id = $_GET['id'];
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v4.0.1">
    <title>Demo Straßenbau</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/dashboard/">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js" integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew==" crossorigin=""></script>

    <!-- Bootstrap core CSS -->
<link href="css/bootstrap.css" rel="stylesheet">

    <style>
    .p0 {
      height:5vh !important;
    }
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }
      .leaflet-popup-content{width:300px !important;}

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
      #main{
        padding:0 !important;
      }
      #main .row,#main .container {
        margin:0 !important;
        padding:0 !important;
      }
      #mapid{
        height:95vh !important;
        margin:0 !important;
        padding:0 !important;
      }
      td{
        font-size:16px;
        font-weight: 500;
        padding: 0.025rem !important;
        border-top:0!important;
      }
      .btn-success{
        margin:5px;
      }
hr {
  margin-top: 0.1rem;
  margin-bottom: 0.1rem;
  border: 0;
  border-top: 3px solid rgba(0, 0, 0, 0.1);
}
.auto {
height: 95vh;
overflow: auto;
width: 200px;
border-left: 1px solid #000;
padding: 10px;
}
    </style>
    <!-- Custom styles for this template -->
    <link href="dashboard.css" rel="stylesheet">
  </head>
  <body>
    <nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow">
  <a class="navbar-brand col-md-3 col-lg-2 mr-0 px-3" href="#">Demo Straßenbau</a>
  <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-toggle="collapse" data-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <input class="form-control form-control-dark w-100" type="text" placeholder="Search" aria-label="Search">
  <ul class="navbar-nav px-3">
    <li class="nav-item text-nowrap">
      <a class="nav-link" href="#"><span data-feather="search"></span></a>
    </li>
  </ul>
</nav>
<div class="container-fluid">
  <div class="row">
    <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
      <div class="sidebar-sticky pt-3">
        <ul class="nav flex-column">
          <li class="nav-item">
            <a class="nav-link" href="index.php">
              <span data-feather="file-text"></span>
              Protokolle
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link active" href="#">
              <span data-feather="map">(current)</span>
              Karte
            </a>
          </li>
        </ul>
      </div>
    </nav>

    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4" id="main">
      
  <div class="row">
    <div class="col-7" id="mapid"></div>
    <div class="col-5 auto">
    <?php
    $response = file_get_contents('https://gis-camp.undefined-solutions.de/api.php?action=getPruefbericht&format=json&id='.$id);
    $response = json_decode($response, true);
    
    $responseZyklen = file_get_contents('https://gis-camp.undefined-solutions.de/api.php?action=getZyklus&format=json&id='.$id);
    $responseZyklen = json_decode($responseZyklen, true);
  
    $responseMesswerte = file_get_contents('https://gis-camp.undefined-solutions.de/api.php?action=getMesswerte&format=json&id='.$id);
    $responseMesswerte = json_decode($responseMesswerte, true);
    
    ?>
<table class="table">
<tr><td colspan="2"><h2>Plattendruckversuch</h2></td></tr>
<tr><td colspan="2"><?php echo $response[measuretype].' - '.(intval($response["plattendurchmesser"])/10);?></td></tr>
<tr><td>Hersteller</td><td><?php echo $response[hersteller];?></td></tr>
<tr><td>Kalibriert am:</td><td><?php echo $response[kalibrierungdatum];?></td></tr>
<tr><td>Gerätenummer:</td><td><?php echo $response[geraetenr];?></td></tr>
<tr><td>Hebearm:</td><td>1:<?php echo $response[hebearm];?> Induktiver Wegtaster</td></tr>
<tr><td>Datum (Beginn-Ende):</td><td><?php echo $response[beginn].' - '.substr($response[ende], -8);?></td>
<tr class="hr"><td colspan="2"><hr></td></tr>
<tr><td>Auftraggeber/Firma:</td><td><input type="text" class="form-control" id="auftraggeber"value ="<?php echo $response[auftraggeber];?>"></td></tr>
<tr><td>Vorhabern/Auftrag/Lage:</td><td><input type="text" class="form-control" id="vorhangenauftraglage"value ="<?php echo $response[vorhangenauftraglage];?>"></td></tr>
<tr><td>Bodenart:</td><td><input type="text" class="form-control" id="bodenart"value ="<?php echo $response[bodenart];?>"></td></tr>
<tr><td>Ausgleichsschicht:</td><td><input type="text" class="form-control" id="ausgleichsschicht"value ="<?php echo $response[ausgleichsschicht];?>"></td></tr>
<tr><td>Bemerkungen:</td><td><input type="text" class="form-control" id="bemerkungen"value ="<?php echo $response[bemerkungen];?>"></td></tr>
<tr><td>Witterung/Temperatur:</td><td><input type="text" class="form-control" id="witterungtemperatur"value ="<?php echo $response[witterungtemperatur];?>"></td></tr>
<tr><td>Prüfpersonal:</td><td><input type="text" class="form-control" id="pruefpersonal"value ="<?php echo $response[pruefpersonal];?>"></td></tr>

  <input type="hidden" name="id" value="<?php echo $id ?>">
<tr><td colspan="2" style="text-align: center;">

<form id="form-id" method="get" action="https://gis-camp.undefined-solutions.de/api.php">
  <input type="hidden" name="action" value="update">
  <input type="hidden" name="id" value="<?php echo $id; ?>">
  <input type="hidden" id="hiddenAuftraggeber" name="auf" value="">
  <input type="hidden" id="hiddenVorhangenauftraglage" name="vor" value="">
  <input type="hidden" id="hiddenBodenart" name="bod" value="">
  <input type="hidden" id="hiddenAusgleichsschicht" name="aus" value="">
  <input type="hidden" id="hiddenBemerkungen" name="bem" value="">
  <input type="hidden" id="hiddenWitterungtemperatur" name="wit" value="">
  <input type="hidden" id="hiddenPruefpersonal" name="pru" value="">

<button class="btn btn-success" onclick="insertData()">Speichern</button>
</form>
<script>
  function insertData() {
    document.getElementById("hiddenAuftraggeber").value = document.getElementById("auftraggeber").value;
    document.getElementById("hiddenVorhangenauftraglage").value = document.getElementById("vorhangenauftraglage").value;
    document.getElementById("hiddenBodenart").value = document.getElementById("bodenart").value;
    document.getElementById("hiddenAusgleichsschicht").value = document.getElementById("ausgleichsschicht").value;
    document.getElementById("hiddenBemerkungen").value = document.getElementById("bemerkungen").value;
    document.getElementById("hiddenWitterungtemperatur").value = document.getElementById("witterungtemperatur").value;
    document.getElementById("hiddenPruefpersonal").value = document.getElementById("pruefpersonal").value;
    document.getElementById('form-id').submit();
  }
</script>
</td></tr>
<tr class="hr"><td colspan="2"><hr></td></tr>
<tr><td>s: <?php echo $response[s];?>mm/cm</td><td>&sigma;: <?php echo $response[sigma];?>kN/m²/cm</td></tr>
<tr><td colspan="2" style="text-align:center">ToDo<br><img src="img/beispielGraph.JPG" width=200px></td></tr>
<tr><td>Ev1: <?php echo bcdiv($response[ev1],1,2);?> MN/m²</td><td>ks = <?php echo $response[ks];?> MN/m³ (d=<?php echo $response[ksd];?>)</td></tr>
<tr><td>Ev2: <?php echo bcdiv($response[ev2],1,2);?> MN/m²</td><td>Ev2/Ev1: <?php echo bcdiv($response[ev2ev1],1,2);?> MN/m²</td></tr>
<tr class="hr"><td colspan="2"><hr></td></tr>
<tr>
<td colspan="2">
<table class="table">
  <tr><td>Nr.</td><td>&sigma;(MN/m²)</td><td>s (mm)</td></tr>
  <tr><td colspan="3">Erstbelastung</td></tr>
  <?php
  foreach ($responseMesswerte as $row) {
    echo "<tr><td>".$row[nummer]."</td><td>".bcdiv($row[sigma],1,4)."</td><td>".bcdiv($row[s],1,2)."</td></tr>";
    if ($row[nummer] == 5){
      echo '<tr><td colspan="3">Entlastung</td></tr>';
    } elseif ($row[nummer] == 7){
      echo '<tr><td colspan="3">Zweitbelastung</td></tr>';
    } 
  }
  ?>
</table>
</td>
</tr>
<tr class="hr"><td colspan="2"><hr></td></tr>
<tr><td colspan="2">Parameter:</td></tr>
<tr><td colspan="2">
<table class="table">
  <tr><td></td><td>Erstbelastung</td><td>Zweitbelastung</td></tr>
  <tr><td>a0:</td><td><?php echo bcdiv($responseZyklen[0][a0],1,4); ?></td><td><?php echo bcdiv($responseZyklen[2][a0],1,4); ?></td></tr>
  <tr><td>a1:</td><td><?php echo bcdiv($responseZyklen[0][a1],1,4); ?></td><td><?php echo bcdiv($responseZyklen[2][a1],1,4); ?></td></tr>
  <tr><td>a2:</td><td><?php echo bcdiv($responseZyklen[0][a2],1,4); ?></td><td><?php echo bcdiv($responseZyklen[2][a2],1,4); ?></td></tr>
</table>
</td></tr>
    </tr>
</table>
<iframe id="ifrm1" name="ifrm1" style="display:none"></iframe>
    </div>
  </div>

    </main>
  </div>
</div>
<script>

	var mymap = L.map('mapid').setView([51.133333333333, 10.416666666667], 6);

	L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
		maxZoom: 18,
		attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
			'<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
			'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
		id: 'mapbox/streets-v11',
		tileSize: 512,
		zoomOffset: -1
	}).addTo(mymap);

  
  <?php
  $responseGeoJSON = file_get_contents('https://gis-camp.undefined-solutions.de/api.php?action=getSingle&format=json&id='.$id);
  $responseGeoJSON = json_decode($responseGeoJSON, true);
  if (isset ($responseGeoJSON[geojson])){
    echo ' var geojson = '. $responseGeoJSON[geojson] . ';';  
    echo ' var marker = L.geoJSON(geojson); ';
    echo ' marker.addTo(mymap); ';
    echo ' mymap.fitBounds(marker.getBounds()); ';  
  }
  
  ?>

</script>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
      <script>window.jQuery || document.write('<script src="../assets/js/vendor/jquery.slim.min.js"><\/script>')</script><script src="js/bootstrap.bundle.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.9.0/feather.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>
        <script src="dashboard.js"></script></body>
</html>
