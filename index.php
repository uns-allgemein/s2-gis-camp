<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v4.0.1">
    <title>Demo Straßenbau</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/dashboard/">

    <!-- Bootstrap core CSS -->
<link href="css/bootstrap.css" rel="stylesheet">

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
      table svg{
        color: black;
        width: 25px !important;
        height: 25px !important;
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="dashboard.css" rel="stylesheet">
  </head>
  <body>
    <nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow">
  <a class="navbar-brand col-md-3 col-lg-2 mr-0 px-3" href="#">Demo Straßenbau</a>
  <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-toggle="collapse" data-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <input class="form-control form-control-dark w-100" type="text" placeholder="Search" aria-label="Search">
  <ul class="navbar-nav px-3">
    <li class="nav-item text-nowrap">
      <a class="nav-link" href="#"><span data-feather="search"></span></a>
    </li>
  </ul>
</nav>
<div class="container-fluid">
  <div class="row">
    <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
      <div class="sidebar-sticky pt-3">
        <ul class="nav flex-column">
          <li class="nav-item">
            <a class="nav-link active" href="#">
              <span data-feather="file-text">(current)</span>
              Protokolle
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="karte.php">
              <span data-feather="map"></span>
              Karte
            </a>
          </li>
        </ul>
      </div>
    </nav>

    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Übersicht</h1><!--
        <div class="btn-toolbar mb-2 mb-md-0">
          <div class="btn-group mr-2">
            <button type="button" class="btn btn-sm btn-outline-secondary">Share</button>
            <button type="button" class="btn btn-sm btn-outline-secondary">Export</button>
          </div>
          <button type="button" class="btn btn-sm btn-outline-secondary dropdown-toggle">
            <span data-feather="calendar"></span>
            diese Woche
          </button>
        </div>-->
      </div>

      <!--<canvas class="my-4 w-100" id="myChart" width="900" height="180"></canvas>-->
      <table class="table table-striped table-sm">
        <thead>
          <tr>
            <th width=1%></th>
            <th>Prüfverfahren</th>
            <th>Auftraggeber</th>
            <th>Zeitraum</th>
            <th>Vorhabern</th>
            <th>Prüfpersonal</th>
            <th>Ev1</th>
            <th>Ev2</th>
            <th>Ev2/Ev1</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
        <?php
        $response = file_get_contents('https://gis-camp.undefined-solutions.de/api.php?action=getOverview&format=json');
        $response = json_decode($response, true);
        foreach ($response as $row){        
          if (is_null($row["ev2"]) or is_null($row["ev1"]) or is_null($row["ev2ev1"])){
            $tr_class = ' class="table-danger"';
          } elseif (is_null($row["auftraggeber"])){
            $tr_class = ' class="table-warning"';
          } elseif ($row["auftraggeber"] == '' or $row["vorhangenauftraglage"] == '' or $row["pruefpersonal"] == ''){
            $tr_class = ' class="table-warning"';
          } else{
            $tr_class = '';
          }
          $auftraggeber = $row["auftraggeber"];
          if ($auftraggeber == ''){
            $auftraggeber = '<center><span data-feather="edit" onclick="window.open(\'edit.php?id='.$row["idreport"].'\',\'_self\');"></span></center>';
          }
          $vorhangenauftraglage = $row["vorhangenauftraglage"];
          if ($vorhangenauftraglage == ''){
            $vorhangenauftraglage = '<center><span data-feather="edit" onclick="window.open(\'edit.php?id='.$row["idreport"].'\',\'_self\');"></span></center>';
          }
          $pruefpersonal = $row["pruefpersonal"];
          if ($pruefpersonal == ''){
            $pruefpersonal = '<center><span data-feather="edit" onclick="window.open(\'edit.php?id='.$row["idreport"].'\',\'_self\');"></span></center>';
          }
          $geom = $row["geom"];
          if ($geom == ''){
            $view = '<span data-feather="eye" onclick="window.open(\'single.php?id='.$row["idreport"].'\',\'_self\');"></span>';
          } else {
            $view = '<span data-feather="map" onclick="window.open(\'single.php?id='.$row["idreport"].'\',\'_self\');"></span>';
          }
          
          echo '<tr'.$tr_class.'>
          <td><input class="" type="checkbox" value="" id="defaultCheck1"></td>
          <td>'.$row["measuretype"].' - '.(intval($row["plattendurchmesser"])/10).'</td>
          <td>'.$auftraggeber.'</td>
          <td>'.$row["beginn"].' - '.substr($row["ende"], -8).'</td>
          <td>'.$vorhangenauftraglage.'</td>
          <td>'.$pruefpersonal.'</td>
          <td>'.$row["ev1"].'</td>
          <td>'.$row["ev2"].'</td>
          <td>'.$row["ev2ev1"].'</td>
          <td><span data-feather="edit" onclick="window.open(\'edit.php?id='.$row["idreport"].'\',\'_self\');"></span> '.$view.' <span data-feather="trash-2" style="margin-left:20px;" onclick="deleteData('.$row["idreport"].');"></span></td>
        </tr>';
        }
        ?>
        <!-- 
          <a href="img/Protokoll.pdf" download="demo.pdf"><span data-feather="printer"></span></a> 
          <span data-feather="download" onclick="alert(\'download Excel-File\');"></span>
          <span data-feather="box" onclick="alert(\'download BIM-File\');"></span>
        -->
        </tbody>
      </table>
      <button type="button" class="btn btn-primary">Auf Karte anzeigen <span data-feather="map"></span></button>
      <button type="button" class="btn btn-primary">Exportieren <span data-feather="download"></span></button>
  </div>
</div>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
      <script>window.jQuery || document.write('<script src="../assets/js/vendor/jquery.slim.min.js"><\/script>')</script><script src="js/bootstrap.bundle.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.9.0/feather.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>
        <script src="dashboard.js"></script>
        <script>
          function deleteData($id) {
            var r = confirm('Wollen Sie diesen Datensatz löschen?');;
            if (r == true) {
              fetch('https://gis-camp.undefined-solutions.de/api.php?action=deleteProtokoll&id='+$id).then(response=>{
                return response.json();
              });
              window.open('index.php','_self');
            }
          }
          </script>

  </body>
</html>
