<?php
$id = $_GET['id'];
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v4.0.1">
    <title>Demo Straßenbau</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/dashboard/">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js" integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew==" crossorigin=""></script>

    
	<link rel="stylesheet" href="dist/MarkerCluster.css" />
	<link rel="stylesheet" href="dist/MarkerCluster.Default.css" />
	<script src="dist/leaflet.markercluster-src.js"></script>

    <!-- Bootstrap core CSS -->
<link href="css/bootstrap.css" rel="stylesheet">

    <style>
    .p0 {
      height:5vh !important;
    }
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }
      .leaflet-popup-content{width:300px !important;}

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
      #main{
        padding:0 !important;
      }
      #main .row,#main .container {
        margin:0 !important;
        padding:0 !important;
      }
      #mapid{
        height:95vh !important;
        margin:0 !important;
        padding:0 !important;
      }
      td{
        font-size:16px;
        font-weight: 500;
        padding: 0.025rem !important;
        border-top:0!important;
      }
hr {
  margin-top: 0.1rem;
  margin-bottom: 0.1rem;
  border: 0;
  border-top: 3px solid rgba(0, 0, 0, 0.1);
}
.auto {
height: 95vh;
overflow: auto;
width: 200px;
border-left: 1px solid #000;
padding: 10px;
}
.leaflet-popup-content{
  font-size: 15px;
}
    </style>
    <!-- Custom styles for this template -->
    <link href="dashboard.css" rel="stylesheet">
  </head>
  <body>
    <nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow">
  <a class="navbar-brand col-md-3 col-lg-2 mr-0 px-3" href="#">Demo Straßenbau</a>
  <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-toggle="collapse" data-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <input class="form-control form-control-dark w-100" type="text" placeholder="Search" aria-label="Search">
  <ul class="navbar-nav px-3">
    <li class="nav-item text-nowrap">
      <a class="nav-link" href="#"><span data-feather="search"></span></a>
    </li>
  </ul>
</nav>
<div class="container-fluid">
  <div class="row">
    <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
      <div class="sidebar-sticky pt-3">
        <ul class="nav flex-column">
          <li class="nav-item">
            <a class="nav-link" href="index.php">
              <span data-feather="file-text"></span>
              Protokolle
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link active" href="#">
              <span data-feather="map">(current)</span>
              Karte
            </a>
          </li>
        </ul>
      </div>
    </nav>

    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4" id="main">
      <div id="mapid" class="col-md-12">
      </div>
    </main>
  </div>
</div>
<script>

	var mymap = L.map('mapid').setView([51.133333333333, 10.416666666667], 6);

	L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
		maxZoom: 18,
		attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
			'<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
			'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
		id: 'mapbox/streets-v11',
		tileSize: 512,
		zoomOffset: -1
	}).addTo(mymap);
  <?php
  $responseGeoJSON = file_get_contents('https://gis-camp-api.undefined-solutions.de/?action=getOverviewMap&format=json');
  $responseGeoJSON = json_decode($responseGeoJSON, true);
  echo ' var geoJsonData = '. $responseGeoJSON[json_build_object] . ';';  
  ?>

  
  
  
  var markers = L.markerClusterGroup();

var geoJsonLayer = L.geoJson(geoJsonData, {
  onEachFeature: function (feature, layer) {
    layer.bindPopup(feature.properties.measuretype + " - " + ((feature.properties.plattendurchmesser)/10)+"<br>"+
    "<b>GPS:</b> "+feature.geometry.coordinates[0]+", "+feature.geometry.coordinates[1] + ", " + feature.properties.alt +"<br>"+
    "<b>Zeitraum:</b> "+feature.properties.beginn.replace("T", " ") + " - " + feature.properties.ende.slice(11)+"<br>"+
    "<b>Auftraggeber:</b> " +feature.properties.auftraggeber + "<br>"+
    "<b>vorhaben:</b> " +feature.properties.auftraggeber + "<br>"+
    "<a href='edit.php?id="+feature.id+"'>Bearbeiten</a> <a href='single.php?id="+feature.id+"' style='padding-left:20px'>Bericht Anzeigen</a>");
  }
});
markers.addLayer(geoJsonLayer);

mymap.addLayer(markers);
  
  //var geojson = L.geoJSON(featureCollection).addTo(mymap);

</script>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
      <script>window.jQuery || document.write('<script src="../assets/js/vendor/jquery.slim.min.js"><\/script>')</script><script src="js/bootstrap.bundle.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.9.0/feather.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>
        <script src="dashboard.js"></script></body>
</html>
