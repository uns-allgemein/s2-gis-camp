<?php
$action = $_GET['action'];
$format = $_GET['format'];
$id = $_GET['id'];


class SQL {
    private $dsn = "pgsql:host=195.128.101.62;port=25432;dbname=strassenbau";
    private $user = "mike";
    private $passwd = "giscamp2020";
    private $outArray = [];

    // Universelle Funktion
    // erstellt die Verbindung zur Datenbank
    function getPDO() {
        return new PDO($this->dsn, $this->user, $this->passwd);
    }

    function doSqlQuery($sql){
        $pdo = $this->getPDO();
        $pdo->query($sql);
        //echo $sql;
        
    }

    // Universelle Funktion
    // Liefert ein OutArray eines SQL Statements
    function getOutArray($sql) {
        $pdo = $this->getPDO();
        $outArray = [];
        foreach ($pdo->query($sql) as $row){
            $rowArray = [];
            foreach($row as $key => $item){
                if (gettype($key)==string){
                    $rowArray[$key] = $item;
                }
            }
            array_push($outArray, $rowArray);
        }
        return $outArray;
    }

    // Universelle Funktion
    // liefert nur die werte des Arrays zurück
    function getSelectedArrayItems($inArray, $fields){
        $outArray = [];
        foreach($inArray as $row){
            $rowArray = [];
            foreach ($row as $key => $value) {
                if (in_array($key, $fields)) {
                    if (isset($value) and ($key == "ev1" or $key == "ev2" or $key == "ev2ev1")){
                        $rowArray[$key] = bcdiv($value,1,2);
                    } else{
                        $rowArray[$key] = $value;
                    }
                }
            }
            array_push($outArray, $rowArray);
        }
        return $outArray;
    }

    // SQL Aufrufe

    function deleteProtokoll($id){
        $this->doSqlQuery("DELETE FROM zyklen WHERE reportID = ".$id.";");
        $this->doSqlQuery("DELETE FROM messwerte WHERE measureID = ".$id.";");
        $this->doSqlQuery("DELETE FROM messung WHERE reportID = ".$id.";");
        $this->doSqlQuery("DELETE FROM pruefbericht WHERE idreport = ".$id.";");
    }
    function getMesswerte($id){
        $this->outArray = $this->getOutArray("SELECT * FROM messwerte WHERE measureid = ".$id);
    }
    function update($id){
        $auftraggeber = $_GET['auf'];
        $vorhangenauftraglage = $_GET['vor'];
        $bodenart = $_GET['bod'];
        $ausgleichsschicht = $_GET['aus'];
        $bemerkungen = $_GET['bem'];
        $witterungtemperatur = $_GET['wit'];
        $pruefpersonal = $_GET['pru'];

        $sql = "UPDATE pruefbericht SET
        auftraggeber = '".$auftraggeber."',
        vorhangenauftraglage = '".$vorhangenauftraglage."',
        bodenart = '".$bodenart."',
        ausgleichsschicht = '".$ausgleichsschicht."',
        bemerkungen = '".$bemerkungen."',
        witterungtemperatur = '".$witterungtemperatur."',
        pruefpersonal = '".$pruefpersonal."'
        WHERE idreport =".$id.";";
        $this->doSqlQuery($sql);

        echo "<script type=\"text/javascript\">window.open(\"single.php?id=".$id."\",\"_self\");</script>";
    }
    
    function getPruefbericht($id){
        $this->outArray = $this->getOutArray("SELECT * FROM pruefbericht INNER JOIN messung ON pruefbericht.idreport=messung.idmeasure WHERE idreport= ".$id)[0];
    }
    function getZyklus($id){
        $this->outArray = $this->getOutArray("SELECT * FROM zyklen WHERE reportid = ".$id." ORDER by index ASC");
    }

    function getOverview(){
        $inArray = $this->getOutArray("SELECT * FROM pruefbericht INNER JOIN messung ON pruefbericht.idreport=messung.idmeasure");
        $fields = array('idreport','geom','geom','measuretype','plattendurchmesser','auftraggeber','beginn','ende','vorhangenauftraglage','pruefpersonal','ev1','ev2','ev2ev1');
        $this->outArray = $this->getSelectedArrayItems($inArray, $fields);
    }
    function getSingle($id){
        $this->outArray = $this->getOutArray("SELECT ST_AsGeoJSON(geom) AS geojson FROM pruefbericht WHERE idreport = ".$id)[0];
    }

    // Return Funktionen
    function returnJSON(){
        return json_encode($this->outArray);
    }
    function returnCSV(){
        $outString = "";
        $outArray = $this->outArray;
        foreach($outArray[0] as $key => $item){
            $outString = $outString . $key . ',';
        }
        $outString = substr($outString, 0, -1).'<br>';
        foreach($outArray as $row){
            foreach($row as $key => $item){
                $outString = $outString . $item . ',';
            }
            $outString = substr($outString, 0, -1).'<br>';
        }
        return $outString;
    }
}
$sql = new SQL;
//action

if ($action == 'getOverview'){
    $sql->getOverview();
} elseif ($action == 'getMesswerte'){
    $sql->getMesswerte($id);
} elseif ($action == 'getPruefbericht'){
    $sql->getPruefbericht($id);
} elseif ($action == 'deleteProtokoll'){
    $sql->deleteProtokoll($id);
} elseif ($action == 'getZyklus'){
    $sql->getZyklus($id);
} elseif ($action == 'getSingle'){
    $sql->getSingle($id);
} elseif ($action == 'update'){
    $sql->update($id);
}
//output Format
if ($format == "csv"){
    echo $sql->returnCSV();
} elseif ($format == "json"){
    echo $sql->returnJSON();
} 
?>